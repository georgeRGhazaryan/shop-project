#George Ghazaryan 04.11.2018

from Shop import Shop
from Admin import Admin


class User:
    #def __init__(self):

    @classmethod
    def initUser(self):

        self.options = {
                        '1': self.displayList,
                        '2': self.buyItem,
                        '3': self.backToMain
                        }

        Shop.initShop()
        self.startMenu()


    @classmethod
    def callUserMenu(self, option):
      if option not in self.options:
         print("Invalid input\n")
         self.startMenu()
         # return
      self.options[option]()


    @classmethod
    def startMenu(self):
      print("\n1. Display Item List\n"
            "2. Buy\n"
            "3. Back to Main Menu")

      self.callUserMenu(input("\nChoose Option. _"))


    @classmethod
    def displayList(self):
      print("\nShop Item list")
      Shop.displayShop()
      self.startMenu()


    @classmethod
    def buyItem(self):
        id = Admin.inputIntFun("\nWrite ID of item to Buy\nID = ") - 1

        if Shop.checkIfItemExist(id):
            Shop.displaySingleItem(id)

            if Shop.checkItemQuantity(id):
                quantity = Admin.inputIntFun("\nHow Many = ")
                Shop.buyItem(id, quantity)
                #self.startMenu()
            else:
                print("\nSorry there is no any of this item right now.\n")
            self.startMenu()
        else:
            print("No Item found with this ID\n")
            self.startMenu()


    @classmethod
    def backToMain(self):
        print("\nLog out from User")