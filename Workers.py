import json
import sys

from Api import Api

class Worker:
    def __init__(self, id = None, name = None, surname = None, salary = None):
        self.id = id
        self.name = name
        self.surname = surname
        self.salary = salary


    @classmethod
    def initWorkers(self):

        self.dbName = "WorkersData"
        self.data, connError = Api.get(self.dbName)

        timeTry = 0
        while connError == 404:
            timeTry += 1

            if timeTry <= 3:

                inp = input("\nError {}.\nNo (Workers) DataBase File Found or Network Error\nTry to reconnect? and create new DataBase (y/n) _".format(timeTry))

                while inp.lower() != 'y' and inp.lower() != 'n':
                    inp = input("\n(y/n only) _ ")

                if inp == 'y':
                    Api.createLocalAndUpload(self.dbName, self.data, connError)
                    self.data, connError = Api.get(self.dbName)
                else:
                    print("\nExiting Program.\nBye!")
                    sys.exit()
            else:
                print("\n__________________________________________________\n")
                print("There is problem with Server or Network")
                print("Try a bit later or check your internet connection")
                print("__________________________________________________\n")
                print("Exiting Program.")
                sys.exit()


        self.workerList = []
        for item in self.data:
            self.workerList.append(Worker(item["id"], item["name"], item["surname"], item["salary"]))


    @classmethod
    def checkIfWorkerExist(self, id):
        self.initWorkers()

        for item in self.workerList:
            if id == item.id:
                return True
        return False


    @classmethod
    def displayWorkers(self):

        self.initWorkers()

        print("\nID    Name", " " * 8, "Surname", " " * 8, "Salary")
        print("____________________________________________")

        for worker in self.workerList:
            print("{:<6d}{:<14s}{:<17s}{:<10.2f}$".format(worker.id + 1, worker.name, worker.surname, worker.salary))


    @classmethod
    def displaySingleWorker(self, id):

        self.initWorkers()

        print("\nID    Name", " " * 8, "Surname", " " * 8, "Salary")
        print("____________________________________________")

        for worker in self.workerList:
            if worker.id == id:
                print("{:<6d}{:<14s}{:<17s}{:<10.2f}$".format(worker.id + 1, worker.name, worker.surname, worker.salary))


    @classmethod
    def addWorker(self,worker):

        if len(self.workerList) == 0:
            newID = 0
        else:
            newID = self.workerList[len(self.workerList)-1].id + 1

        worker.id = newID
        #self.workerList.append(worker)
        self.data.append(worker.__dict__)

        ifDone, err = Api.createLocalAndUpload(self.dbName, self.data)
        if ifDone:
            print("Worker Added!\n")
        else:
            print(err)


    @classmethod
    def removeWorker(self, id):

        for item in self.data:
            if item["id"] == id:
                self.data.remove(item)

        ifDone, err = Api.createLocalAndUpload(self.dbName, self.data)
        if ifDone:
            print("Done! Worker Removed\n")
        else:
            print(err)
