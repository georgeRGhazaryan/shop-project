import sys

from Item import Item
from Api import Api

class Shop:
    #def __init__(self):

    @classmethod
    def initShop(self):

        self.dbName = "Shopdata"
        self.data, connError = Api.get(self.dbName)

        timeTry = 0
        while connError == 404:
            timeTry += 1

            if timeTry <= 3:

                inp = input("\nError {}.\nNo (Item) DataBase File Found or Network Error\nTry to reconnect? and create new DataBase (y/n) _".format(timeTry))

                while inp.lower() != 'y' and inp.lower() != 'n':
                    inp = input("\n(y/n only) _ ")

                if inp == 'y':
                    Api.createLocalAndUpload(self.dbName,self.data,connError)
                    self.data, connError = Api.get(self.dbName)
                else:
                    print("\nExiting Program.\nBye!")
                    sys.exit()
            else:
                print("\n__________________________________________________\n")
                print("There is problem with Server or Network")
                print("Try a bit later or check your internet connection")
                print("__________________________________________________\n")
                print("Exiting Program.")
                sys.exit()

        self.itemList = []
        for item in self.data:
            self.itemList.append(Item(item["id"], item["name"], item["price"], item["quantity"]))


    @classmethod
    def checkIfItemExist(self,id):
        self.initShop()

        for item in self.itemList:
            if id == item.id:
                return True
        return False


    @classmethod
    def displayShop(self):

        self.initShop()

        print("\nID    Name", " " * 8, "Price ($)", " " * 8,"Quantity")
        print("___________________________________________")

        for item in self.itemList:
            print("{:<7d}{:<13s} $ {:<18.2f}{:<10d}".format(item.id+1,item.name, item.price,item.quantity))
        print()



    @classmethod
    def displaySingleItem(self,id):

        self.initShop()

        print("\nID    Name", " " * 8, "Price ($)", " " * 8, "Quantity")
        print("____________________________________________")

        for item in self.itemList:
            if id == item.id:
                print("{:<7d}{:<15s} $ {:<18.2f}{:<10d}".format(item.id + 1, item.name, item.price, item.quantity))


    @classmethod
    def addItem(self, item):

        if len(self.itemList) == 0:
            newID = 0
        else:
            newID = self.itemList[len(self.itemList)-1].id + 1

        item.id = newID
        #self.itemList.append(item)
        self.data.append(item.__dict__)

        ifDone , err = Api.createLocalAndUpload(self.dbName,self.data)
        if ifDone:
            print("Item Added!\n")
        else:
            print(err)


    @classmethod
    def editItem(self,id,name,price,quantity):

        for item in self.data:
            if item["id"] == id:
                item["name"] = name
                item["price"] = price
                item["quantity"] = quantity


        ifDone, err = Api.createLocalAndUpload(self.dbName,self.data)
        if ifDone:
            print("Done!\n")
        else:
            print(err)


    @classmethod
    def buyItem(self, id, quantity):

        for item in self.data:
            if item["id"] == id:
                if item["quantity"] >= quantity:
                    item["quantity"] -= quantity
                    print("\nPurchase Done!\n")
                else:
                    print("\nSorry not enough {}. I can offer maximum {}".format(item["name"]+"s",item["quantity"]))
                    inp = input("\nBuy x{} of {}? (y/n) _ ".format(item["quantity"],item["name"]+"s"))

                    while inp.lower() != 'y' and inp.lower() != 'n':
                        inp = input("\n(y/n only) _ ")

                    if inp =='y':
                        item["quantity"] = 0
                        print("\nPurchase Done!\n")
                    else:
                        print("\nPurchase Canceled.\n")
                        return

            ifDone, err = Api.createLocalAndUpload(self.dbName,self.data)


    @classmethod
    def checkItemQuantity(self, id):

        for item in self.data:
            if item["id"] == id:
                if item["quantity"] != 0:
                    return True
        return False


    @classmethod
    def removeItem(self, id):

        for item in self.data:
            if item["id"] == id:
                self.data.remove(item)

        ifDone, err = Api.createLocalAndUpload(self.dbName,self.data)
        if ifDone:
            print("Done! Item Removed\n")
        else:
            print(err)
