#George Ghazaryan 03.11.2018

import sys
from Admin import Admin
from User import User
from ApiLogin import ApiLogin


class MainMenu():
   #def __init__(self):

   @classmethod
   def initMainMenu(self):
      self.options = {'1': self.adminMenu,
                       '2': self.userMenu,
                       '3': self.exitProgram}

      self.startMenu()

   @classmethod
   def callMenu(self, option):
      if option not in self.options:
         print("Invalid input\n")
         self.startMenu()
         #return

      self.options[option]()


   @classmethod
   def startMenu(self):
      print("\n1. Admin account\n"
            "2. User\n"
            "3. Exit")

      self.callMenu(input("\nChoose Option. _"))


   @classmethod
   def adminMenu(self):
      login = input("Administrator (login_ Admin / password_ admin123)\nLogin_")
      password = input("Password_")

      if ApiLogin.checkAdminPassword(login,password):
         Admin.initAdmin()
         self.startMenu()
      else:
         self.startMenu()


   @classmethod
   def userMenu(self):
      print("\nWelcome to our Shop")
      User.initUser()
      self.startMenu()

   def exitProgram(self):
      print("Exit Byee..")
      sys.exit()


MainMenu.initMainMenu()