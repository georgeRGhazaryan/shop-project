import urllib.request, json
import ftplib
import os.path

class Api():

    @classmethod
    def get(self,urlName):
        urlPath = "https://clienttouser.000webhostapp.com/" + urlName
        try:
            with urllib.request.urlopen(urlPath) as url:
                data = json.loads(url.read().decode())
                return data, 200
        except Exception as e:
            return e, 404


    @classmethod
    def post(self,dbName):
        try:
            ftp = ftplib.FTP('files.000webhost.com', 'clienttouser', 'program123')
            ftp.cwd('/public_html/')
            file = open(dbName, 'rb+')
            try:
                ftp.delete('/public_html/'+dbName)
            except:
                pass
            ftp.storbinary('STOR ' + dbName, file)
            file.close()
            ftp.quit()

            return True, 200
        except Exception as e:
            return False, 400


    @classmethod
    def createLocalAndUpload(self,dbName, data, type=0):

        # Create local file
        if type == 404:
            if not os.path.isfile(dbName):
                f = open(dbName, 'w')
                a = '[]'
                f.write(str(a))
                f.close()
        else:
            with open(dbName, 'w') as f:
                json.dump(data, f)

        # upload
        return Api.post(dbName)