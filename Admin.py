#George Ghazaryan 03.11.2018

from Shop import Shop
from Workers import Worker
from Item import Item

class Admin:
    #def __init__(self):

    @classmethod
    def initAdmin(self):

        self.options = {
                        '1': self.displayList,
                        '2': self.addItem,
                        '3': self.editItem,
                        '4': self.removeItem,
                        '5': self.displayWorkers,
                        '6': self.addWorker,
                        '7': self.removeWorker,
                        '8': self.backToMain
                        }

        Shop.initShop()
        Worker.initWorkers()
        self.startMenu()

    @classmethod
    def callAdminMenu(self, option):
      if option not in self.options:
         print("Invalid input\n")
         self.startMenu()
         # return
      self.options[option]()


    @classmethod
    def startMenu(self):
      print("\nYou are in Admin Form\n\n"
            "1. Show all Items\n"
            "2. Add Item\n"
            "3. Edit Item\n"
            "4. Remove Item\n"
            "5. Show all Workers\n"
            "6. Add Worker\n"
            "7. Remove Worker\n"
            "8. Back to Main Menu")

      self.callAdminMenu(input("\nChoose Option. _"))


    @classmethod
    def displayList(self):
        print("\nShop Item list\n")
        Shop.displayShop()
        self.startMenu()


    @classmethod
    def addItem(self):
        print("Details of Item\n")
        item = Item()
        item.name = input("Item Name = ")
        item.price = self.inputFloatFun("Item Price = ")
        item.quantity = self.inputIntFun("Item Quantity = ")
        Shop.addItem(item)
        self.startMenu()


    @classmethod
    def editItem(self):
        id = self.inputIntFun("\nWrite ID of item to Edit\nID = ")-1

        if Shop.checkIfItemExist(id):
            Shop.displaySingleItem(id)
            print("\nEdit Details of Item")
            name = input("\nItem Name = ")
            price = self.inputFloatFun("Item Price = ")
            quantity = self.inputIntFun("Item Quantity = ")
            Shop.editItem(id, name, price, quantity)
            self.startMenu()
        else:
            print("No Item found with this ID\n")
            self.startMenu()


    @classmethod
    def removeItem(self):
        id = self.inputIntFun("\nWrite ID of item to Remove\nID = ")-1

        if Shop.checkIfItemExist(id):
            Shop.displaySingleItem(id)
            Shop.removeItem(id)
            self.startMenu()
        else:
            print("No Item found with this ID\n")
            self.startMenu()


    @classmethod
    def displayWorkers(self):
        Worker.displayWorkers()
        self.startMenu()


    @classmethod
    def addWorker(self):
        print("Details of Worker\n")
        worker = Worker()
        worker.name = input("Name = ")
        worker.surname = input("Surname = ")
        worker.salary = self.inputFloatFun("Salary = ")
        Worker.addWorker(worker)
        self.startMenu()


    @classmethod
    def removeWorker(self):
        id = self.inputIntFun("\nWrite ID of worker to Remove\nID = ") - 1

        if Worker.checkIfWorkerExist(id):
            Worker.displaySingleWorker(id)
            Worker.removeWorker(id)
            self.startMenu()
        else:
            print("No Worker found with this ID\n")
            self.startMenu()

    @classmethod
    def backToMain(self):
        print("\nLog out from Admin\n")


    @classmethod
    def inputIntFun(self,inputText):

        inputName = input(inputText)

        while not inputName.isdigit():
            print("Oops... Only positive numbers")
            inputName = input(inputText)

        return int(inputName)


    @classmethod
    def inputFloatFun(self, inputText):

        inputName = input(inputText)

        while not inputName.replace(".", "", 1).isdigit():
            print("Oops... Only Int or Float numbers")
            inputName = input(inputText)

        return float(inputName)
