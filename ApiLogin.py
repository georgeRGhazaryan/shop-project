from urllib.parse import urlencode
from urllib.request import Request, urlopen


class ApiLogin():

    @classmethod
    def checkAdminPassword(self,uName,pWord):

        url = 'https://clienttouser.000webhostapp.com/admin.php'
        post = {'username': uName, 'password': pWord}

        request = Request(url, urlencode(post).encode())
        json = urlopen(request).read().decode()

        if json == "true":
            print("\nSuccess! Login and Password Correct.")
            return True
        else:
            print("\nWrong Login and Password!")
            return False